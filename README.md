# Zer0Paper_CloudOps_Assessment

## Name
Zer0Paper CloudOps Assessment

## Description
An assessment to demonstrate my CloudOps proficiency.

## Roadmap

- Create the server in a private VPC subnet, including any other cloud resource(s) that are required to run the application.
- Run the checker script.
